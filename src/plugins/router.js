import Vue from 'vue'
import VueRouter from 'vue-router'
import MainContent from "@/components/MainContent";
import ExamplePage from "@/components/ExamplePage";

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', name: 'Index', component: MainContent },
    {
      path: '/example',
      component: ExamplePage
    }
  ],
});

export default router
